load data local infile "C:\\Users\\kaan selim\\Desktop\\Data_Base\\company_data\\customers.csv"
into table customers
fields terminated by ';'
ignore 1 lines
;
select * from customers;

select count(customers.CustomerID) as number_of_customers, customers.Country
from customers
group by customers.Country
order by number_of_customers desc
;

select count(products.ProductID) as product_id, suppliers.SupplierName
from products join suppliers on products.SupplierID = suppliers.SupplierID
group by products.SupplierID
order by count(products.ProductID) desc
;

