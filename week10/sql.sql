describe customers;
select * from customers order by CustomerName desc;

create index abc on customers(CustomerName);

select * from customers order by CustomerName desc;
alter table customers drop index abc;
create view tmp as select * from customers order by CustomerName desc;
select * from tmp;
insert into tmp values (279,"selim","kağan","muğla","kötekli","asd","türkiye");
select * from tmp;